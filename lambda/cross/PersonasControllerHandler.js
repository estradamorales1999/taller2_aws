const daoManager = require("./../utilities/daoManager");
const sqsManager = require("./../utilities/sqsManager");
const util = require("./../utilities/common");
const auditoryDto = require("../contracts/auditoryDto");

const personasTable = process.env.TABLA_PERSONAS;
const colaauditoria = process.env.TABLE_AUDITORY;

module.exports.handler = async (event, context) => {
   
    try {
        var data = JSON.parse(event.body);
        var msgId = context.awsRequestId; 


        var infoAuditory = new auditoryDto(msgId, data);
        await sqsManager.sendMessage(context, infoAuditory, colaauditoria);

        var params = { TableName : personasTable, Item: data };
        util.insertLog("Objeto para BD: " + JSON.stringify(params));
        
        

        const response = await daoManager.register(context,params);
        util.insertLog("Result Insert BD: " + JSON.stringify(response));

        
        infoAuditory.setResponse(response);
        await sqsManager.sendMessage(context, infoAuditory, colaauditoria);

        return response;
    } catch (err) {
        util.insertLog("Error en personasControllerHandler: " + err);
        return util.cargaMensaje(500,"" + err);
    }     
};
