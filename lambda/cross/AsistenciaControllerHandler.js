const daoManager = require("./../utilities/daoManager");
const sqsManager = require("./../utilities/sqsManager");
const util = require("./../utilities/common");
const auditoryDto = require("../contracts/auditoryDto");

const AsistenciaTable = process.env.TABLA_ASISTENCIA;
const colaauditoria = process.env.TABLE_AUDITORY;
const personasTable = process.env.TABLA_PERSONAS;

module.exports.saveAsistence = async (event, context) => {
    try {
        var res=null;
        var rgx=new Date();
        var request=JSON.parse(event.body)
        console.log(request.idestudiante);
        console.log(request)
        var data = JSON.parse(event.body);
        var params = { TableName : AsistenciaTable, Item: data };
        console.log("Record created")
        var isactive=await daomanager.searchByIdentification(context,personasTable, "identificacion",request.idestudiante);
        console.log(isactive);
        if(!isactive)
        {
            console.log("is in the if")
            res= {
                statusCode: 200,
                body: JSON.stringify("Usted no puede asistir a este curso, està inactivo"),
            };
        return res;
        }
        else
        {
            var messagesqs=new messageRequestDto(
                rgx.getFullYear()+rgx.getMonth()+rgx.getDate()+rgx.getHours()+rgx.getMinutes()+rgx.getSeconds()+rgx.getMilliseconds()+"","Asistencia registrada",rgx
                );      
            const response = await daomanager.register(context,params);
            const resultAudit = await daosqs.sendMessage(context, messagesqs,"sqs-application-dev-AuditoryQueue",3);
            return response;
        }
    } 
    catch (err) {
        console.log(err)
        return "Error"+err
    }  
};

