const daoManager = require("./../utilities/daoManager");
const sqsManager = require("./../utilities/sqsManager");
const util = require("./../utilities/common");
const auditoryDto = require("../contracts/auditoryDto");

const capacitacionTable = process.env.TABLA_CAPACITACION;
const colaauditoria = process.env.TABLE_AUDITORY;

module.exports.handler = async (event, context) => {
    try {

        var data = null;
        var params = null;
        var rgx=new Date();
        data = JSON.parse(event.body);
        //var split1=JSON.parse(event.body)
        //var a=event.body.fecha_programacion.getMonth()+1;
        //var div=event.body.fecha_programacion.getFullYear()+"-"+a+"-"+event.body.fecha_programacion.getDate()
        
        var month=rgx.getMonth()+1;
        var date1=new Date();
        
        
        var messagesqs=new messageRequestDto(
            rgx.getFullYear()+rgx.getMonth()+rgx.getDate()+rgx.getHours()+rgx.getMinutes()+rgx.getSeconds()+rgx.getMilliseconds()+"","capacitacion registrada",rgx
            );
       var aux=new Date(); 
       var entereddate=new Date(data.fecha_programacion);

       aux.setHours(0,0,0,0);
       entereddate.setHours(0,0,0,0);
       //aux.setHours(-5);
       //entereddate.setHours(-5);
       console.log(aux)
       console.log(entereddate)
        if(entereddate.getTime() < aux.getTime())
        {
            //var data = JSON.parse(event.body);
            data.estado="finalizado";
        }

        else if(entereddate.getTime()==aux.getTime())
        {
            data.estado="programado";
        }

        else if(entereddate.getTime() > aux.getTime())
        {
            data.estado="pospuesto";
        }
        
        console.log(data)
        params = { TableName : capacitacionTable, Item: data };
        const response = await daomanager.register(context,params);
        const resultAudit=await daosqs.sendMessage(context, messagesqs,"sqs-application-dev-AuditoryQueue",3);
        console.log("arrived here")
        return response;
    } catch (err) {
        console.log(err)
        return "Error"+err
    }  
};
