
class auditoryDto {
  constructor(id, request){
    var currentDate = new Date().toISOString();
    this.id = id;
    this.registerDate = currentDate;
    this.request  = request;
    this.response  = "";
    this.responseDate  = "";

  }

  setResponse(resp){
    var currentDate = new Date().toISOString();
    this.response = resp;
    this.responseDate = currentDate;
  }
};

module.exports = auditoryDto;